<?php
/**
 * Created by Glenn Harding using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   ado
 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
 * @copyright 2015 Movehut
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace app\Models;
/**
 * Class EquipmentType
 *
 * @category app\Models
 * @package  ado
 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class EquipmentType extends \ADODB_Active_Record
{
    public $equipment_type_id;
    public $equipment_type_name;
    public $_table = "equipment_types";
    public $class_name = "\\app\\Models\\EquipmentType";
    /**
    * Value of member equipment_type_name
    *
    * @return mixed value of member
    */
    public function getEquipmentTypeName()
    {
        return $this->equipment_type_name;
    }

    public function setRelations()
    {
        $equipment = new Equipment();
        self::classHasMany($this->class_name, 'equipment', 'equipment_type');
        self::TableHasMany($this->_table, $equipment->_table, 'equipment_id');
        self::TableKeyHasMany($this->_table, 'equipment_type_id', $equipment->_table, 'equipment_id');
    }
    /**
    * Set the value of equipment_type_name member
    *
    * @param mixed $val
    *
    * @return $this
    */

    public function setEquipmentTypeName($val)
    {
        $this->equipment_type_name = $val;

        return $this;
    }

    /**
    * Value of member equipment_type_id
    *
    * @return mixed value of member
    */
    public function getEquipmentTypeId()
    {
        return $this->equipment_type_id;
    }

    public function getClassName()
    {
        return $this->class_name;
    }

    public function createTable()
    {
        app('AdoDb')->Execute("CREATE TABLE IF NOT EXISTS `equipment_types` (
            `equipment_type_id` INT NOT NULL AUTO_INCREMENT,
            `equipment_type_name` VARCHAR(100) NOT NULL,
            PRIMARY KEY (`equipment_type_id`)
            ) ENGINE=MyISAM;
        ");
        return true;
    }

}
