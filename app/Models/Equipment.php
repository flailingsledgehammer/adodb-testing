<?php
/**
 * Created by Glenn Harding using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   ado
 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
 * @copyright 2015 Movehut
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace app\Models;

use \ADODB_Active_Record;
/**
 * Class Equipment
 *
 * @category app\Models
 * @package  ado
 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class Equipment extends \ADODB_Active_Record
{
    public $_table = "equipment";
    public $equipment_id;
    public $developer_id;
    public $equipment_type;
    public $class_name = "\\app\\Models\\Equipment";


    /**
     * Value of member equipment_type
     *
     * @return integer value of member
     */
    public function getEquipmentType()
    {
        return $this->equipment_type;
    }

    public function setRelations()
    {
        self::ClassBelongsTo($this->class_name, 'developer', 'developer_id', 'developer_id');
        self::ClassBelongsTo($this->class_name, 'equipment_type', 'equipment_type', 'equipment_type_id');
        self::TableBelongsTo($this->_table, 'developer', 'developer_id', 'developer_id');
        self::TableBelongsTo($this->_table, 'equipment_type', 'equipment_type', 'equipment_type_id');
        self::TableKeyBelongsTo($this->_table, 'developer_id', 'developer', 'developer_id');
        self::TableKeyBelongsTo($this->_table, 'equipment_type', 'equipment_type', 'equipment_type_id');
        $developer = null;
        $equipment_type = null;
        unset($developer);
        unset($equipment_type);
    }


    /**
     * Set the value of equipment_type member
     *
     * @param integer $val
     *
     * @return $this
     */
    public function setEquipmentType($val)
    {
        $this->equipment_type = $val;

        return $this;
    }


    /**
     * Value of member developer_id
     *
     * @return integer value of member
     */
    public function getDeveloperId()
    {
        return $this->developer_id;
    }


    /**
     * Set the value of developer_id member
     *
     * @param integer $val
     *
     * @return $this
     */
    public function setDeveloperId($val)
    {
        $this->developer_id = $val;

        return $this;
    }


    /**
     * Value of member equipment_id
     *
     * @return mixed value of member
     */
    public function getEquipmentId()
    {
        return $this->equipment_id;
    }

    public function getClassName()
    {
        return $this->class_name;
    }
    public function createTable()
    {
        app('AdoDb')->Execute("CREATE TABLE IF NOT EXISTS `equipment`
            ( `equipment_id` INT NOT NULL AUTO_INCREMENT,
            `developer_id` INT(11) NOT NULL,
            `equipment_type` INT(11) NOT NULL,
            PRIMARY KEY (`equipment_id`),
            FOREIGN KEY (`developer_id`)
            REFERENCES `adoTest`.`developers` (`developer_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
            FOREIGN KEY (`equipment_type`)
            REFERENCES equipment_types(`equipment_type_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
            ) ENGINE=MyISAM;
        ");
        return true;
    }

}
