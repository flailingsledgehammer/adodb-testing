<?php
/**
 * Created by Glenn Harding using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Abstract Class
 * @package   ado
 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
 * @copyright 2015 Movehut
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace app\Models;
use \ADODB_Active_Record;
/**
 * Abstract Class ArAdapter
 *
 * @category app\Models
 * @package  ado
 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
abstract class ArAdapter extends ADODB_Active_Record
{
    public function __construct()
    {
        parent::__construct();
        $this->setRelations();
    }

    /**
     * function to set up the relations of the class/table/tablekey
     * @return true
     */
    public abstract function setRelations();
}
