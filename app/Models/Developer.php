<?php
/**
 * Created by Glenn Harding using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   ado
 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
 * @copyright 2015 Movehut
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace app\Models;

/**
 * Class Developer
 *
 * @category app\Models
 * @package  ado
 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class Developer extends \ADODB_Active_Record
{
    public $developer_id;
    public $first_name;
    public $last_name;
    public $_table = 'developers';
    private $class_name = "\\app\\Models\\Developer";
    /**
     * Value of member developer_id
     *
     * @return integer value of member
     */
    public function getDeveloperId()
    {
        return $this->developer_id;
    }

    /**
     * Value of member first_name
     *
     * @return string value of member
     */
    public function getFirstName()
    {

        return $this->first_name;
    }


    /**
     * Set the value of first_name member
     *
     * @param string $val
     *
     * @return $this
     */
    public function setFirstName($val)
    {
        $this->first_name = $val;

        return $this;
    }


    /**
     * Value of member last_name
     *
     * @return string value of member
     */
    public function getLastName()
    {
        return $this->last_name;
    }


    /**
     * Set the value of last_name member
     *
     * @param string $val
     *
     * @return $this
     */
    public function setLastName($val)
    {
        $this->last_name = $val;

        return $this;
    }

    public function setRelations()
    {
        $equipment = new Equipment();
        self::ClassHasMany($this->class_name, $equipment->_table, 'developer_id');
        self::TableHasMany($this->_table, $equipment->_table, 'developer_id');
        self::TableKeyHasMany($this->_table, 'developer_id', $equipment->_table, 'developer_id');
    }

    public function getClassName()
    {
        return $this->class_name;
    }

    public function createTable()
    {
        app('AdoDb')->Execute("CREATE TABLE IF NOT EXISTS `developers` (
          `developer_id` INT NOT NULL AUTO_INCREMENT,
          `first_name` VARCHAR(100) NOT NULL,
          `last_name` VARCHAR(100) NOT NULL,
          PRIMARY KEY (`developer_id`)
        ) ENGINE=MyISAM;
        ");
        return true;
    }
}

