<?php
/**
 * Created by Glenn Harding using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   ado
 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
 * @copyright 2015 Movehut
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace App\Http\Controllers;

use App\Models\Developer;
use app\Models\Equipment;
use app\Models\EquipmentType;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class SiteController
 *
 * @category App\Http\Controllers
 * @package  ado
 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class SiteController extends BaseController
{
    public function index()
    {
        if (view()->exists('site.index')) {
            return view('site.index');
        }
    }

    public function createDevelopers()
    {
        // create the table
        (new Developer())->createTable();
        $faker = \Faker\Factory::create();
        $i = 0;
        while ($i < 4) {
            $dev = new Developer();
            $dev->setFirstName($faker->firstName);
            $dev->setLastName($faker->lastName);
            $dev->save();
            $i++;
        }
        echo "done!";
    }

    public function getDeveloper()
    {
        $developer = new Developer();

        // using explicit values
        $developer->load('developer_id=1');

        print_r($developer);

        // using tokenised input
        $developer->load('developer_id=?',array('1'));

        print_r($developer);
    }

    public function createExtraTables()
    {
        (new EquipmentType)->createTable();
        (new Equipment)->createTable();
    }

    public function createEquipmentTypes()
    {
        (new \App\Models\EquipmentType)->createTable();

        $faker = \Faker\Factory::create();
        $i = 0;
        while ($i < 10) {
            $equip_type = new \App\Models\EquipmentType();
            $equip_type->setEquipmentTypeName($faker->word);
            $equip_type->save();
            $i++;
        }
    }

    public function createEquipment()
    {
        // safely create the table in case it has not yet been created
        (new \App\Models\Equipment)->createTable();

        $i = 0;
        while ($i < 100) {
            $equipment = new \App\Models\Equipment();
            $equipment->setEquipmentType(mt_rand(1, 10));
            $equipment->setDeveloperId(mt_rand(1,4));
            $equipment->save();
            $i++;
        }
        echo "Done!";
    }

    public function getDevEquipment()
    {
        $dev = new Developer();
        $dev->load('developer_id=1');
        echo "<pre>";
        print_r($dev->loadRelations('equipment'));
    }
}
