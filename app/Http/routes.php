<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$app->get('/', function () use ($app) {
//    return $app->welcome();
//});

$app->get('/', 'SiteController@index');
$app->get('/createDevelopers', 'SiteController@createDevelopers');
$app->get('/getDeveloper', 'SiteController@getDeveloper');
$app->get('/createExtraTables', 'SiteController@createExtraTables');
$app->get('/createEquipmentTypes', 'SiteController@createEquipmentTypes');
$app->get('/createEquipment', 'SiteController@createEquipment');
$app->get('/getDevEquipment', 'SiteController@getDevEquipment');