<?php
/**
 * Created by Glenn Harding using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  File
 * @package   ado
 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
 * @copyright 2015 Glenn Harding
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
?>
<head>
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdn.rawgit.com/google/code-prettify/master/styles/desert.css"/>
</head>
<body>
    <div class="container">
        <h1>ORM Testing</h1>

        <div>
            <p>This is a simple web application created using Lumen to demonstrate the abilities on ADODB as a potential ORM for future use.</p>
        </div>
        <div>
            <h2>ADOdb</h2>
            <p>The initial ADOdb Object should look something like the following: </p><br/>
            <pre class="prettyprint lang-php">
                    <?php print_r(app('AdoDb'));?>
            </pre>
            <p>As you can see, this object is connected via mysqli to a schema called adoTest.</p>
            <br/>
            <h3>Initialisation within Lumen Framework</h3>
            <p>For the sake of total completion, I will include instructions on how I got this up and running within a framework - in this case Lumen.
                This section will therefore talk about how the ADOdb object has been created and how it can be called within the Lumen framework.
                The first thing to know is that we should include all of the files that we will be requiring from the adodb package into our codebase. In this case, I have included these at the top of the <code>/bootstrap/app.php</code> file.
                <pre class="prettyprint lang-php">

                    require_once __DIR__.'/../vendor/autoload.php';

                    // Autoload any required classes not covered otherwise
                    require_once(__DIR__.'/../app/Components/adodb/adodb.inc.php');
                    require_once(__DIR__.'/../app/Components/adodb/adodb-active-record.inc.php');

                    Dotenv::load(__DIR__.'/../');
                </pre>
                However we could abstract this further and use a separate includes file.
            </p>
            <p>
                The second thing to know is that in Lumen components of the main app should be created as ServiceProviders in the <code>/app/Providers/</code> folder.
                These providers extend from the default provider provided by the Lumen framework, which is namespaced as <code>Illuminate\Support\ServiceProvider</code>.
                Because of the way in which ADOdb needs to be configured we will be registering an instance that the provider will initialise. This is demonstrated below:
            </p>
            <pre class="prettyprint lang-php">

                /**
                 * Created by Glenn Harding using PhpStorm.
                 *
                 * PHP version 5
                 *
                 * Licensed under the Apache License, Version 2.0 (the "License");
                 * you may not use this file except in compliance with the License.
                 * You may obtain a copy of the License at
                 *
                 *     http://www.apache.org/licenses/LICENSE-2.0
                 *
                 * Unless required by applicable law or agreed to in writing, software
                 * distributed under the License is distributed on an "AS IS" BASIS,
                 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
                 * See the License for the specific language governing permissions and
                 * limitations under the License.
                 *
                 * @category  Class
                 * @package   ado
                 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
                 * @copyright 2015 Movehut
                 *
                 * For the full copyright and license information, please view the LICENSE.md
                 * file that was distributed with this source code.
                 *
                 */

                namespace app\Providers;

                use Illuminate\Support\ServiceProvider;

                /**
                 * Class AdoDbServiceProvider
                 *
                 * @category app\Providers
                 * @package  ado
                 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
                 *
                 * For the full copyright and license information, please view the LICENSE.md
                 * file that was distributed with this source code.
                 *
                 */
                class AdoDbServiceProvider extends ServiceProvider
                {
                    public function register()
                    {
                        $this->app->instance('AdoDb', ADONewConnection(env('DB_CONNECTION')));
                    }

                    public function boot()
                    {
                        app('AdoDb')->debug = env('APP_DEBUG');
                        app('AdoDb')->Connect(env('DB_HOST'), env('DB_USERNAME'), env('DB_PASSWORD'), env('DB_DATABASE'));
                    }
                }
            </pre><br/>
            <p>Once the provider has been written, it must then be registered within the <code>/bootstrap/app.php</code> file under the register service providers section. This requires one line of code: </p>
            <pre class="prettyprint lang-php">

                $app->register(App\Providers\AdoDbServiceProvider::class);
            </pre>
            <p>Now that we have registered our AdoDbServiceProvider, we can use it by calling the object through the lumen app object using the hook that is specified in the service provider (in this case 'AdoDb') like so:</p>
            <pre class="prettyprint lang-php">

                app('AdoDB');
            </pre><br/>
            <p>Once all of the service providers in our <code>/bootstrap/app.php</code> file have been registered, their respective <code>boot()</code> functions are called. In the case of our AdoDbServiceProvider, this will register a database connection with the instance.</p>
            <p>When all of this is completed, we finally have to call the static function <code>SetDatabaseAdapter</code> on the <code>ADODB_Active_Record class</code>.</p>
            <pre class="prettyprint lang-php">

                 $app->register(App\Providers\AppServiceProvider::class);
                 $app->register(App\Providers\EventServiceProvider::class);
                 $app->register(App\Providers\AdoDbServiceProvider::class);
                 \ADODB_Active_Record::SetDatabaseAdapter(clone(app('AdoDb')));
            </pre>
            <h3>General Use</h3>
            <hr>
            <h4>Setting up Active Record</h4>
            <p>
                Let us assume that the database is currently empty of tables or data and that we want to keep a list of the developers currently working in a web design shop. The following code would allow us to create that table.
            </p>
            <pre class="lang-php prettyprint">

                        app('AdoDb')->Execute("CREATE TABLE IF NOT EXISTS `developers` (
                            `developer_id` INT NOT NULL AUTO_INCREMENT,
                            `first_name` VARCHAR(100) NOT NULL,
                            `last_name` VARCHAR(100) NOT NULL,
                            PRIMARY KEY (`developer_id`)
                            ) ENGINE=MyISAM;
                        ");
            </pre><br/>
            <p>The Execute function shown here allows SQL commands to be issued to the database that our <code>AdoDb</code> object is pointing to. This can be used to execute <code>SELECT</code> queries, <code>UPDATE</code> and <code>INSERT</code> functions etc.</p>

            <p>So now we have our developers table. We now want to be able to use that table in a sensible OO way. <code>ADOdb</code> implements this through the Active Record pattern. To use this we employ the <code>ADOdb_Active_Record</code> class and extend from it to create an AR class for our table.</p>
            <pre class="prettyprint lang-php">
                /**
                 * Created by Glenn Harding using PhpStorm.
                 *
                 * PHP version 5
                 *
                 * Licensed under the Apache License, Version 2.0 (the "License");
                 * you may not use this file except in compliance with the License.
                 * You may obtain a copy of the License at
                 *
                 *     http://www.apache.org/licenses/LICENSE-2.0
                 *
                 * Unless required by applicable law or agreed to in writing, software
                 * distributed under the License is distributed on an "AS IS" BASIS,
                 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
                 * See the License for the specific language governing permissions and
                 * limitations under the License.
                  *
                 * @category  Class
                 * @package   ado
                 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
                 * @copyright 2015 Movehut
                 *
                 * For the full copyright and license information, please view the LICENSE.md
                 * file that was distributed with this source code.
                 *
                 */

                namespace app\Models;

                /**
                 * Class Developer
                 *
                 * @category app\Models
                 * @package  ado
                 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
                 *
                 * For the full copyright and license information, please view the LICENSE.md
                 * file that was distributed with this source code.
                 *
                 */
                class Developer extends \ADODB_Active_Record
                {
                    public $developer_id;
                    public $first_name;
                    public $last_name;
                    public $_table = 'developers';
                    private $class_name = "\\app\\Models\\Developer";
                    /**
                     * Value of member developer_id
                     *
                     * @return integer value of member
                     */
                    public function getDeveloperId()
                    {
                        return $this->developer_id;
                    }

                    /**
                     * Value of member first_name
                     *
                     * @return string value of member
                     */
                    public function getFirstName()
                    {

                        return $this->first_name;
                    }


                    /**
                     * Set the value of first_name member
                     *
                     * @param string $val
                     *
                     * @return $this
                     */
                    public function setFirstName($val)
                    {
                        $this->first_name = $val;

                        return $this;
                    }


                    /**
                     * Value of member last_name
                     *
                     * @return string value of member
                     */
                    public function getLastName()
                    {
                        return $this->last_name;
                    }


                    /**
                     * Set the value of last_name member
                     *
                     * @param string $val
                     *
                     * @return $this
                     */
                    public function setLastName($val)
                    {
                        $this->last_name = $val;

                        return $this;
                    }

                    public function setRelations()
                    {
                        $equipment = new Equipment();
                        self::ClassHasMany($this->class_name, $equipment->_table, 'developer_id');
                        self::TableHasMany($this->_table, $equipment->_table, 'developer_id');
                        self::TableKeyHasMany($this->_table, 'developer_id', $equipment->_table, 'developer_id');
                    }

                    public function getClassName()
                    {
                        return $this->class_name;
                    }

                    public function createTable()
                    {
                        app('AdoDb')->Execute("CREATE TABLE IF NOT EXISTS `developers` (
                          `developer_id` INT NOT NULL AUTO_INCREMENT,
                          `first_name` VARCHAR(100) NOT NULL,
                          `last_name` VARCHAR(100) NOT NULL,
                          PRIMARY KEY (`developer_id`)
                        ) ENGINE=MyISAM;
                        ");
                        return true;
                    }
                }
            </pre><br/>
            <p>In the above example, we have created a new <code>ADOdb_Active_Record</code> class <code>Developer</code> to access the developers table. <code>Zend_Db_DataObject</code> takes the name of the class, pluralises it (according to American English rules :/ ), and assumes that this is the name of the table in the database. Also note that with MySQL/MariaDb, table names are case-sensitive, so your class name must match the table name's class. With other database implementations where tables are case-insensitive your class can be capitalised differently.

                This kind of behaviour is typical of <code>ADOdb_Active_Record</code>. It will assume as much as possible by convention rather than explicit configuration. In situations where it isn't possible to use the conventions that <code>ADOdb_Active_Record</code> expects, options can be specified to override this behaviour. In this case we will assume that we are using a MySQL database, and that we do in fact need to specify the table name to the object because of our differing capitalisation between the class and the table.

                <code>ADOdb</code> provides two ways of setting our own table name for our class/objects. The first is to pass the table name as an argument to the constructor of an object:
                <br/>
            </p>
            <pre class="prettyprint lang-php">

                $developer = new Developer('developers');
            </pre>
            <br/>
            <p>
            This is quite a dirty way of doing things, as every time you wish to create an object of the <code>Developer</code> class you would be required to pass this argument.

            The second method is to define the table name as a variable within the class called <code>$_table</code>:
            </p>
            <pre class="prettyprint lang-php">

                class Developer extends ADODB_Active_Record
                {
                    public $developer_id;
                    public $first_name;
                    public $last_name;
                    public $_table = 'developers';
            </pre>
            <br/>
            <hr>
            <h4>Performing Operations Using AR</h4>
            <p>

                Now that we have an AR class linked to a database table, we will now want to insert our list of developers into the developer table. The following example, which has been built as a controller action that can be accessed <a href="/createDevelopers">here</a>, demonstrates how this is possible using <code>ADODB_Active_Record</code> and the classes that extend it.
            </p>
            <br/>
            <pre class="lang-php prettyprint">

                public function createDevelopers()
                {
                    // for demo purposes, create the table in case it doesn't exist yet
                    (new Developer())->createTable();
                    $faker = \Faker\Factory::create();
                    $i = 0;
                    while ($i < 4) {
                        $dev = new Developer();
                        $dev->setFirstName($faker->firstName);
                        $dev->setLastName($faker->lastName);
                        $dev->save();
                        $i++;
                    }
                    echo "done!";
                }
            </pre>
            <br/>
            <p>
                If you're used to working with AR patterns (I know we all are because we've done it with Yii), this will be very familiar and natural to you. Which works as an advantage of this ORM to us.
            </p><br/>
            <p>
                Once we've saved records to the table, we can recover them using <code>ADODB_Active_Record</code>'s <code>Load()</code> function (example <a href="">here</a>):
            </p><br/>
            <pre class="lang-php prettyprint">

                public function getDeveloper()
                {
                    $developer = new Developer();

                    // using explicit values
                    $developer->load('developer_id=1');

                    print_r($developer);

                    // using tokenised input
                    $developer->load('developer_id=?',array('1'));

                    print_r($developer);
                }
            </pre>
            <br/>
            <p>
                Both of the above methods of record retrieval will return the something that looks like this:
            </p><br/>
            <pre class="lang-php prettyprint">

                app\Models\Developer Object ( [developer_id] => 1 [first_name] => Kevin [last_name] => Thompson [_table] => developers [_dbat] => 0[_tableat] => developers [_where] => developer_id=? [_saved] => 1 [_lasterr] => [_original] => Array ( [0] => 1 [1] => Kevin [2] => Thompson ) [foreignName] => app\models\developer [lockMode] => for update )
            </pre>
            <br/>
            <hr>
            <h3>Object-Relational Mapping</h3>
            <p>
                Object-Relational Mapping is very easily achieved using <code>ADOdb</code>. Since <code>ADOdb 5.06</code>, parent child relationships have been supported by the plugin. The relationships are established within a codebase via the <code>ClassHasMany()</code>, <code>ClassBelongsTo()</code>, <code>TableHasMany()</code>, <code>TableBelongsTo()</code>, <code>TableKeyHasMany()</code> and <code>TableKeyBelongsTo()</code> static functions of the <code>ADODB_Active_Record</code> class.

                We'll use the developers from the previous section on AR in our next scenario. Lets say that the developers are all being issued new pieces of equipment and the company has decided that they want to keep track of who has been given what. As a result, two more tables are required within the database: one to store the information on the equipment and who has been assigned it, and another to store the different types of equipment that can be given out.
                This means then that we need to create two new tables and two new models and establish the relationships between them.
            </p>
            <br/>
            <h4>Creating the models</h4>
            <p>
            First we'll create our models, starting with equipment: </p>
            <br/>
            <pre class="prettyprint lang-php">

                /**
                 * Created by Glenn Harding using PhpStorm.
                 *
                 * PHP version 5
                 *
                 * Licensed under the Apache License, Version 2.0 (the "License");
                 * you may not use this file except in compliance with the License.
                 * You may obtain a copy of the License at
                 *
                 *     http://www.apache.org/licenses/LICENSE-2.0
                 *
                 * Unless required by applicable law or agreed to in writing, software
                 * distributed under the License is distributed on an "AS IS" BASIS,
                 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
                 * See the License for the specific language governing permissions and
                 * limitations under the License.
                  *
                 * @category  Class
                 * @package   ado
                 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
                 * @copyright 2015 Movehut
                 *
                 * For the full copyright and license information, please view the LICENSE.md
                 * file that was distributed with this source code.
                 *
                 */

                namespace app\Models;

                use \ADODB_Active_Record;
                /**
                 * Class Equipment
                 *
                 * @category app\Models
                 * @package  ado
                 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
                 *
                 * For the full copyright and license information, please view the LICENSE.md
                 * file that was distributed with this source code.
                 *
                 */
                class Equipment extends \ADODB_Active_Record
                {
                    public $_table = "equipment";
                    public $equipment_id;
                    public $developer_id;
                    public $equipment_type;
                    public $class_name = "\\app\\Models\\Equipment";


                    /**
                     * Value of member equipment_type
                     *
                     * @return integer value of member
                     */
                    public function getEquipmentType()
                    {
                        return $this->equipment_type;
                    }

                    public function setRelations()
                    {
                        $developer = new Developer;
                        $equipment_type = new EquipmentType;
                        self::ClassBelongsTo($this->class_name, 'developer', 'developer_id', 'developer_id');
                        self::ClassBelongsTo($this->class_name, 'equipment_type', 'equipment_type', 'equipment_type_id');
                        self::TableBelongsTo($this->_table, 'developer', 'developer_id', 'developer_id');
                        self::TableBelongsTo($this->_table, 'equipment_type', 'equipment_type', 'equipment_type_id');
                        self::TableKeyBelongsTo($this->_table, 'developer_id', 'developer', 'developer_id');
                        self::TableKeyBelongsTo($this->_table, 'equipment_type', 'equipment_type', 'equipment_type_id');
                        $developer = null;
                        $equipment_type = null;
                        unset($developer);
                        unset($equipment_type);
                    }


                    /**
                     * Set the value of equipment_type member
                     *
                     * @param integer $val
                     *
                     * @return $this
                     */
                    public function setEquipmentType($val)
                    {
                        $this->equipment_type = $val;

                        return $this;
                    }


                    /**
                     * Value of member developer_id
                     *
                     * @return integer value of member
                     */
                    public function getDeveloperId()
                    {
                        return $this->developer_id;
                    }


                    /**
                     * Set the value of developer_id member
                     *
                     * @param integer $val
                     *
                     * @return $this
                     */
                    public function setDeveloperId($val)
                    {
                        $this->developer_id = $val;

                        return $this;
                    }


                    /**
                     * Value of member equipment_id
                     *
                     * @return mixed value of member
                     */
                    public function getEquipmentId()
                    {
                        return $this->equipment_id;
                    }

                    public function getClassName()
                    {
                        return $this->class_name;
                    }
                    public function createTable()
                    {
                        app('AdoDb')->Execute("CREATE TABLE IF NOT EXISTS `equipment`
                            ( `equipment_id` INT NOT NULL AUTO_INCREMENT,
                            `developer_id` INT(11) NOT NULL,
                            `equipment_type` INT(11) NOT NULL,
                            PRIMARY KEY (`equipment_id`),
                            FOREIGN KEY (`developer_id`)
                            REFERENCES `adoTest`.`developers` (`developer_id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION,
                            FOREIGN KEY (`equipment_type`)
                            REFERENCES equipment_types(`equipment_type_id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION
                            ) ENGINE=MyISAM;
                        ");
                        return true;
                    }

                }
            </pre><br/>
            And next we'll create our Equipment type model:
            <pre class="prettyprint lang-php">


                /**
                 * Created by Glenn Harding using PhpStorm.
                 *
                 * PHP version 5
                 *
                 * Licensed under the Apache License, Version 2.0 (the "License");
                 * you may not use this file except in compliance with the License.
                 * You may obtain a copy of the License at
                 *
                 *     http://www.apache.org/licenses/LICENSE-2.0
                 *
                 * Unless required by applicable law or agreed to in writing, software
                 * distributed under the License is distributed on an "AS IS" BASIS,
                 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
                 * See the License for the specific language governing permissions and
                 * limitations under the License.
                  *
                 * @category  Class
                 * @package   ado
                 * @author    Glenn Harding <glenn.harding@movehut.co.uk>
                 * @copyright 2015 Movehut
                 *
                 * For the full copyright and license information, please view the LICENSE.md
                 * file that was distributed with this source code.
                 *
                 */

                namespace app\Models;
                /**
                 * Class EquipmentType
                 *
                 * @category app\Models
                 * @package  ado
                 * @author   Glenn Harding <glenn.harding@movehut.co.uk>
                 *
                 * For the full copyright and license information, please view the LICENSE.md
                 * file that was distributed with this source code.
                 *
                 */
                class EquipmentType extends \ADODB_Active_Record
                {
                    public $equipment_type_id;
                    public $equipment_type_name;
                    public $_table = "equipment_types";
                    public $class_name = "\\app\\Models\\EquipmentType";
                    /**
                    * Value of member equipment_type_name
                    *
                    * @return mixed value of member
                    */
                    public function getEquipmentTypeName()
                    {
                        return $this->equipment_type_name;
                    }

                    public function setRelations()
                    {
                        $equipment = new Equipment();
                        self::classHasMany($this->class_name, 'equipment', 'equipment_type');
                        self::TableHasMany($this->_table, $equipment->_table, 'equipment_id');
                        self::TableKeyHasMany($this->_table, 'equipment_type_id', $equipment->_table, 'equipment_id');
                    }
                    /**
                    * Set the value of equipment_type_name member
                    *
                    * @param mixed $val
                    *
                    * @return $this
                    */

                    public function setEquipmentTypeName($val)
                    {
                        $this->equipment_type_name = $val;

                        return $this;
                    }

                    /**
                    * Value of member equipment_type_id
                    *
                    * @return mixed value of member
                    */
                    public function getEquipmentTypeId()
                    {
                        return $this->equipment_type_id;
                    }

                    public function getClassName()
                    {
                        return $this->class_name;
                    }

                    public function createTable()
                    {
                        app('AdoDb')->Execute("CREATE TABLE IF NOT EXISTS `equipment_types` (
                            `equipment_type_id` INT NOT NULL AUTO_INCREMENT,
                            `equipment_type_name` VARCHAR(100) NOT NULL,
                            PRIMARY KEY (`equipment_type_id`)
                            ) ENGINE=MyISAM;
                        ");
                        return true;
                    }

                }


            </pre><br/>
            <h4>Generating the tables</h4>
            <p>The next step to tackle this task is to create the raw tables to store our objects. To do this, we'll create a new function in  our controller that will allow us to do this.</p>
            <br/>
            <pre class="lang-php prettyprint">

                public function createExtraTables()
                {
                    (new EquipmentType)->createTable();
                    (new Equipment)->createTable();
                }
            </pre><br/>
            <h4>Inserting data</h4>
            We're now ready to insert some data for our scenario. Because of the way that equipment depends on records in equipment_types we will have to insert records into the latter first.
            <pre class="lang-php prettyprint">

                public function createEquipmentTypes()
                {
                    (new EquipmentType)->createTable();

                    $faker = \Faker\Factory::create();
                    $i = 0;
                    while ($i < 10) {
                        $equip_type = new EquipmentType();
                        $equip_type->setEquipmentTypeName($faker->word);
                        $equip_type->save();
                        $i++;
                    }
                }
            </pre><br/>
            We can now insert our data into the Equipment table:
            <pre class="prettyprint lang-php">

                public function createEquipment()
                {
                    // safely create the table in case it has not yet been created
                    (new \App\Models\Equipment)->createTable();

                    $i = 0;
                    while ($i < 100) {
                        $equipment = new \App\Models\Equipment();
                        $equipment->setEquipmentType(mt_rand(1, 10));
                        $equipment->setDeveloperId(mt_rand(1,4));
                        $equipment->save();
                        $i++;
                    }
                    echo "Done!";
                }
            </pre><br/>
            <h4>Setting Up Relations</h4>
            <p>As mentioned previously, we also need to set up the relations between our models. For the purposes of keeping things simple, I have created a function in each of my models so far called <code>setRelations()</code> which takes care of this for me.</p>
            <p>Below is the <code>setRelations()</code> function from my <code>Developer</code> model.</p>
            <pre class="prettyprint lang-php">

                public function setRelations()
                {
                    $equipment = new Equipment();
                    self::ClassHasMany($this->class_name, $equipment->_table, 'developer_id');
                    self::TableHasMany($this->_table, $equipment->_table, 'developer_id');
                    self::TableKeyHasMany($this->_table, 'developer_id', $equipment->_table, 'developer_id');
                }
            </pre>
            <p>Because this model extends from <code>ADODB_Active_Record</code> I can call the <code>HasMany</code> and <code>BelongsTo</code> functions in a static context using the <code>self</code> keyword as these are present in the parent class.</p>
            <p>We can see examples of the BelongsTo functions being called in the Equipment class.</p>
            <pre class="prettyprint lang-php">

                public function setRelations()
                {
                    self::ClassBelongsTo($this->class_name, 'developer', 'developer_id', 'developer_id');
                    self::ClassBelongsTo($this->class_name, 'equipment_type', 'equipment_type', 'equipment_type_id');
                    self::TableBelongsTo($this->_table, 'developer', 'developer_id', 'developer_id');
                    self::TableBelongsTo($this->_table, 'equipment_type', 'equipment_type', 'equipment_type_id');
                    self::TableKeyBelongsTo($this->_table, 'developer_id', 'developer', 'developer_id');
                    self::TableKeyBelongsTo($this->_table, 'equipment_type', 'equipment_type', 'equipment_type_id');
                    $developer = null;
                    $equipment_type = null;
                    unset($developer);
                    unset($equipment_type);
                }
            </pre>
            <p>It is worth noting the inconsistencies between these functions, as the <code>BelongsTo()</code> function set expects to receive the non-plural form of table names, despite what the documentation says.</p>
            <h4>Using The Relations</h4>
            <p>In order to use the relations that we have set up, we must first have them declared globally so that the <code>ADODB_Active_Record</code> class knows that they exist. To do this, we should declare the relations in <code>/bootstrap/app.php</code> like so:</p>
            <pre class="prettyprint lang-php">

                (new \app\Models\Developer)->setRelations();
                (new \app\Models\Equipment)->setRelations();
                (new \app\Models\EquipmentType)->setRelations();
            </pre>
            <p>
                We must declare these relations as the <code>ADODB_Active_Record</code> class needs to know about them in order to fetch the relations for us when we want them. Once this has been done, we can then call the relations of an object.
            </p>
            <pre class="prettyprint lang-php">

                public function getDevEquipment()
                {
                    $dev = new Developer();
                    $dev->load('developer_id=1');
                    print_r($dev->loadRelations('equipment'));
                }
            </pre>
            <p>An example of the output of this can be found <a href="/getDevEquipment">here</a></p>
        </div>
    </div>
</body>