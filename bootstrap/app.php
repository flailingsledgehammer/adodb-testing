<?php

require_once __DIR__.'/../vendor/autoload.php';

// Autoload any required classes not covered otherwise
require_once(__DIR__.'/../app/Components/adodb/adodb.inc.php');
require_once(__DIR__.'/../app/Components/adodb/adodb-active-record.inc.php');

Dotenv::load(__DIR__.'/../');

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

 $app->withFacades();

// $app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//     // Illuminate\Cookie\Middleware\EncryptCookies::class,
//     // Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
//     // Illuminate\Session\Middleware\StartSession::class,
//     // Illuminate\View\Middleware\ShareErrorsFromSession::class,
//     // Laravel\Lumen\Http\Middleware\VerifyCsrfToken::class,
// ]);

// $app->routeMiddleware([

// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

 $app->register(App\Providers\AppServiceProvider::class);
 $app->register(App\Providers\EventServiceProvider::class);
 $app->register(App\Providers\AdoDbServiceProvider::class);
 \ADODB_Active_Record::SetDatabaseAdapter(clone(app('AdoDb')));
/*
|--------------------------------------------------------------------------
| Load The Models
|--------------------------------------------------------------------------
|
| We will now include all of the models so that they can be used anywhere
| in the application. This will allow us to perform operations using objects.
|
 */

$app->group(['namespace' => 'App\Models'], function ($app) {
    $models = glob(__DIR__.'/../app/Models/*.php');
    foreach ($models as $m) {
        require_once($m);
    }
});
/*
|--------------------------------------------------------------------------
| Load the relations
|--------------------------------------------------------------------------
|
| With the models now loaded into the application, we can now specify the
| relational rules between them.
|
*/

(new \app\Models\Developer)->setRelations();
(new \app\Models\Equipment)->setRelations();
(new \app\Models\EquipmentType)->setRelations();

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../app/Http/routes.php';
});

return $app;
